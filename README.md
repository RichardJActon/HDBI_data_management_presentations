# HDBI Data Management Presentations

- [HDBI data manager introduction](https://richardjacton.gitlab.io/HDBI_data_management_presentations/HDBI_data_management.sozi.html)
	- Key Links:
		- [Survey Link](https://cryptpad.fr/form/#/2/form/view/-3jdv2zZ1B3sCWUWNcX19FafGtXoiP+mizN7cAyWJyY/)
		- [FAIR data](https://www.go-fair.org/fair-principles/)
		- [Renku](https://www.renkulab.io)
		- [OMERO](https://www.openmicroscopy.org/omero/)
		- [Registered Reports](https://www.cos.io/initiatives/registered-reports)

So far the presentations in this project have been made with the following free/libre software tools:

- [Inkscape](https://inkscape.org/)
- [Sozi](http://sozi.guide/en/about-sozi.html)

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

